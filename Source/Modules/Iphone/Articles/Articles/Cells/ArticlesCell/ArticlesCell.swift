//
//  ContactsCell.swift
//  ArticlesCell
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

class ArticlesCell: UITableViewCell, ModelTransfer {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbImageView: UIImageView!

    //MARK: - ModelTransfer

    func update(with model: ArticlesCellViewModelProtocol) {
        titleLabel.text = model.title
        thumbImageView.image(withURL: model.imageThumb)
    }
}

protocol ArticlesCellViewModelProtocol {
    var id: Int32 { get }
    var title: String? { get }
    var imageThumb: String? { get }
    init(article: Article)
}

class ArticlesCellViewModel: ArticlesCellViewModelProtocol {
    var id: Int32
    var title: String?
    var imageThumb: String?
    required init(article: Article) {
        self.id = article.id
        self.title = article.title
        self.imageThumb = article.imageThumb
    }
}

