//
//  ArticlesViewController.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

class ArticlesViewController: BaseViewController, DTTableViewManageable {
    @IBOutlet weak var tableView: UITableView!
    fileprivate var viewModel = ArticlesViewModel()
    
    //MARK: - Lifecicle Object
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableManager()
        setupRefreshControl()
        updateUI()
        load()
    }
    
    //MARK: - Methods
    
    override func observeViewModel() {
        viewModel.successHandler = { [weak self] _ in
            self?.updateUI()
        }
        viewModel.errorHandler = { error in
            Alerts.defaultAlert(message: error.localizedDescription)
        }
    }
    
   override func observeState() {
        viewModel.state.asDriver().drive(onNext: { [weak self] (state) in
            self?.view.progressFor(state: state)
        }).disposed(by: disposeBag)
    }
    
    private func setupTableManager() {
        enableSelfSizing()
        manager.startManaging(withDelegate: self)
        manager.register(ArticlesCell.self)
        manager.didSelect(ArticlesCell.self) { [weak self] (_, viewModel, _) in
            self?.openArticle(id: viewModel.id)
        }
    }
    
    private func setupRefreshControl() {
        tableView.refreshControl = createRefreshControl()
        tableView.alwaysBounceVertical = true
        refreshControl?.addTarget(self, action: #selector(load), for: .valueChanged)
    }
    
    private func updateUI() {
        manager.memoryStorage.removeAll()
        let items = viewModel.items
        manager.memoryStorage.setItems(items)
    }
    
    @objc private func load() {
        viewModel.load()
    }

    private func openArticle(id: Int32) {
        let articleViewController = ArticleViewController.instantiate(withArticleId: id)
       articleViewController?.push()
    }
}
