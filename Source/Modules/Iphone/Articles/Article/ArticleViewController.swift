//
//  ArticleViewController.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import MessageUI

extension ArticleViewController {
    static func instantiate(withArticleId id: Int32) -> ArticleViewController? {
        let controller: ArticleViewController? = instantiate()
        controller?.viewModel = ArticleViewModel(id: id)
        return controller
    }
}

class ArticleViewController: BaseViewController {
    @IBOutlet weak var webView: UIWebView!
    var viewModel: ArticleViewModel?
  
    //MARK: - Lifecicle Object
    
    override func viewDidLoad() {
        super.viewDidLoad()
        load()
    }
    
    //MARK: - Methods
    
    override func observeViewModel() {
        viewModel?.successHandler = { [weak self] article in
            self?.updateUI(with: article)
        }
        viewModel?.errorHandler = { error in
            Alerts.defaultAlert(message: error.localizedDescription)
        }
    }
    
    override func observeState() {
        viewModel?.state.asDriver().drive(onNext: { [weak self] (state) in
            self?.view.progressFor(state: state)
        }).disposed(by: disposeBag)
    }
    
    private func load() {
        viewModel?.load()
    }
    
    private func updateUI(with article: Article) {
        guard let string = article.html else { return }
        webView.loadHTMLString(string, baseURL: nil)
    }
    
    //MARK: - Actions
    
    @IBAction func shareTapped(_ sender: UIBarButtonItem) {
        guard let article = viewModel?.article else { return }
        let shareItem = article.export()
        let activityViewController = UIActivityViewController(activityItems: [shareItem], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
}

