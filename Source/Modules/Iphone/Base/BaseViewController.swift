//
//  BaseViewController.swift
//  Article
//
//  Created by Oleksandr Nesynov on 5/23/17.
//  Copyright © 2017 Article. All rights reserved.
//

class BaseViewController: UIViewController {
    @IBOutlet weak var connectionView: ArticlesConnectionView!
    @IBOutlet weak var connectionConstraint: NSLayoutConstraint!
    var refreshControl: UIRefreshControl?
    let disposeBag = DisposeBag()

    //MARK: - Lifecicle Object
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
    }
    
    //MARK: Method (Overide this method if needed)
    
    func updateConstraintsIfNeeded() { }
    
    func observeViewModel() { }
    func observeNotification() { }
    func observeActions() { }
    func observeState() { }
    
    //MARK: Method
    
    private func setupObservers() {
        setupConnectionView()
        observeState()
        observeViewModel()
        observeNotification()
        observeActions()
        observeReachability()
    }
    
    private func setupConnectionView() {
        guard connectionView != nil, connectionConstraint != nil else { return }
        connectionView.constraintHandler = { [weak self] constant in
            self?.connectionConstraint.constant = constant
        }
    }
    
    private func observeReachability() {
        ArticleReachability.shared.isReachable
            .asDriver()
            .drive(onNext: { [weak self] isReachable in
                guard let connectionView = self?.connectionView else { return }
                if isReachable {
                    connectionView.dismiss()
                } else {
                    connectionView.present()
                }
            }).disposed(by: disposeBag)
    }

    func createRefreshControl() -> UIRefreshControl {
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = .gray
        refreshControl?.addTarget(self, action: #selector(refreshValueDidChanged), for: .valueChanged)
        return refreshControl!
    }

    @objc private func refreshValueDidChanged() {
        if (refreshControl?.isRefreshing)! {
            refreshControl?.endRefreshing()
        }
    }
}
