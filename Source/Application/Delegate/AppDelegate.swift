//
//  AppDelegate.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        ArticleReachability.shared.startListening()
        AppAppearance.apply()
        AppConfigurate.apply()
        return true
    }
}
