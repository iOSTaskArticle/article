//
//  ManagedObjectResponseParser.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import CoreData

class ManagedObjectsResponseParser<P: ManagedObjectJSONParsable>: ResponseParsing where P: NSManagedObject {
    
    private var context: NSManagedObjectContext
    private var itemsKey: String?
    
    //MARK: - Lifecicle Object
    
    init(context: NSManagedObjectContext = CoreDataStack.createWorkerContext(),
         itemsKey: String? = nil) {
        self.context = context
        self.itemsKey = itemsKey
    }
    
    //MARK: - Methods
    
    func parse(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) -> Result<[P], APIError> {
        
        guard error == nil else {
            return .failure(APIError(response: response, data: nil, error: error))
        }
        
        guard let json = data?.parse(withOptions: .allowFragments) else {
            return .failure(.parsingFailed)
        }
        
        var array = [JSON]()
        
        if let itemsKey = itemsKey, let arrayValue = json[itemsKey].array {
            array = arrayValue
        } else if let arrayValue = json.array {
            array = arrayValue
        } else {
            return .failure(.parsingFailed)
        }
        
        var objects = Array<P>()
        
        if array.count > 0 {
            context.performAndWait { [unowned self] in
                for case let (item) in array {
                    if let object = P.parse(json: item, context: self.context) as? P {
                        objects.append(object)
                    }
                }
            }
        }
        
        context.saveAsynchronously()
        
        guard objects.count > 0 else {
            return .failure(APIError.emptyResponse)
        }
        
        return .success(objects)
    }
}

class ManagedObjectResponseParser<P: ManagedObjectStringParsable>: ResponseParsing where P: NSManagedObject {
    
    private let id: Int32
    private let context: NSManagedObjectContext
    
    //MARK: - Lifecicle Object
    
    init(id: Int32,
         context: NSManagedObjectContext = CoreDataStack.createWorkerContext()) {
        self.id = id
        self.context = context
    }
    
    //MARK: - Methods
    
    func parse(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) -> Result<P, APIError> {
        
        guard error == nil else {
            return .failure(APIError(response: response, data: nil, error: error))
        }
        
        guard let data = data,
            let string = String(data: data, encoding: String.Encoding.utf8) else {
            return .failure(APIError.parsingFailed)
        }
        
        guard let object = P.parse(id: id, string: string, context: context) as? P else {
            return .failure(APIError.emptyResponse)
        }
        
        context.saveAsynchronously()
        
        return .success(object)
    }
}


