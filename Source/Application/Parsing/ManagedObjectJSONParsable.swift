//
//  ManagedObjectJSONParsable.swift
//  Article
//
//  Created by Admin on 12.01.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

import CoreData

protocol ManagedObjectJSONParsable: NSFetchRequestResult {
    
    associatedtype ParsedType: NSManagedObject = Self
    
    static func parse(json: JSON, context: NSManagedObjectContext) -> ParsedType?
}

protocol ManagedObjectStringParsable: NSFetchRequestResult {
    
    associatedtype ParsedType: NSManagedObject = Self
    
    var id: Int32 { get set }
    
    static func parse(id: Int32, string: String, context: NSManagedObjectContext) -> ParsedType?
}
