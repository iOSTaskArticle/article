//
//  AppConfigurate.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

class AppConfigurate {
    static func apply() {
        setupArticlesAsRoot()
    }
    
    private static func setupArticlesAsRoot() {
        guard let articlesViewController = ArticlesViewController.instantiate() else { return }
        let navigationController = UINavigationController(rootViewController: articlesViewController)
        navigationController.setNavigationBarHidden(false, animated: true)
        AppRouter.rootViewController = navigationController
    }
}
