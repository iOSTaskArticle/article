//
//  APIError.swift
//  Article
//
//  Created by Admin on 12.01.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

public enum Result<Value, ErrorType: Error> {
    case success(Value)
    case failure(ErrorType)
    
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    public var error: ErrorType? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}
