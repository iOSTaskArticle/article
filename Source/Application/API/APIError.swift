//
//  APIError.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

public enum APIError: Error {
    case failure(errorMessage: String?)
    case parsingFailed
    case emptyResponse
    case noInternetConnection
    case unauthorized(errorMessage: String?)
    case notFound(errorMessage: String?)
    
    var message: String? {
        switch self {
        case .failure(let errorMessage):
            return errorMessage
        case .parsingFailed:
            return ""
        case .emptyResponse:
            return nil
        case .noInternetConnection:
            return ""
        case .unauthorized(let errorMessage):
            return errorMessage ?? ""
        case .notFound(let errorMessage):
            return errorMessage
        }
    }
    
    fileprivate static func errorMessage(fromResponse response: HTTPURLResponse?, data: Data?, error: Error?) -> String? {
        if let errorJSON = data?.parse(withOptions: .allowFragments),
            let errorsDictionary = errorJSON["errors"].dictionary {
            var errorMessages = [String]()
            for (key, value) in errorsDictionary {
                errorMessages.append(key + " " + (value.first?.0 ?? ""))
            }
            return (errorMessages as NSArray).componentsJoined(by: "\n")
        }
        
        if let error = error as NSError?, error.code != NSURLErrorCancelled {
            return error.localizedDescription
        }
        
        return nil
    }
    
    init(response: HTTPURLResponse?, data: Data?, error: Error?) {
        if let error = error as NSError?, error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
            self = .noInternetConnection
        } else if response?.statusCode == 401 {
            self = .unauthorized(errorMessage: APIError.errorMessage(fromResponse: response, data: data, error: error))
        } else if response?.statusCode == 404 {
            self = .notFound(errorMessage: APIError.errorMessage(fromResponse: response, data: data, error: error))
        } else {
            self = .failure(errorMessage: APIError.errorMessage(fromResponse: response, data: data, error: error))
        }
    }
}
