//
//  AppCoreDataStack.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import CoreData

class CoreDataStack: NSObject {
    
    //MARK: - Lifecicle Object
    
    override init() {
        super.init()
        observeNotification()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Property Object
    
    static let shared = CoreDataStack()
    
    private static var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls.last!
    }()
    
    static var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "ArticleModel", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    static var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: CoreDataStack.managedObjectModel)
        
        let url = CoreDataStack.applicationDocumentsDirectory.appendingPathComponent("ArticleCoreData.sqlite")
        
        var failureReason = "There was an error creating or loading the application's saved data."
        
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            var dict = [String: Any]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            
            abort()
        }
        
        return coordinator
    }()
    
    static var rootContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.perform {
            managedObjectContext.persistentStoreCoordinator = CoreDataStack.persistentStoreCoordinator
        }
        managedObjectContext.name = "rootContext"
        managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return managedObjectContext
    }()
    
    let mainContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.parent = CoreDataStack.rootContext
        managedObjectContext.name = "mainContext"
        return managedObjectContext
    }()
    
    static func createWorkerContext() -> NSManagedObjectContext {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.parent = CoreDataStack.rootContext
        managedObjectContext.mergePolicy = CoreDataStack.rootContext.mergePolicy
        return managedObjectContext
    }
    
    //MARK: - Methods
    
    @objc private func contextWillSave(nofitication: NSNotification) {
        if let context = nofitication.object as? NSManagedObjectContext {
            context.perform {
                let array = Array(context.insertedObjects)
                if array.count > 0 {
                    do {
                        try context.obtainPermanentIDs(for: array)
                    } catch {}
                }
            }
        }
    }
    
    @objc private func contextDidSave(notification: Notification) {
        if let rootContext = notification.object as? NSManagedObjectContext,
            rootContext == mainContext.parent {
            mainContext.perform {
                self.mainContext.mergeChanges(fromContextDidSave: notification)
            }
        }
    }
    
    private func observeNotification() {
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(CoreDataStack.contextWillSave),
            name: NSNotification.Name.NSManagedObjectContextWillSave,
            object: mainContext
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(CoreDataStack.contextDidSave),
            name: NSNotification.Name.NSManagedObjectContextDidSave,
            object: mainContext.parent
        )
    }
}

