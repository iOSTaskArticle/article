//
//  AppCoreDataProvider.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import CoreData

class CoreDataProvider<Value: NSFetchRequestResult>: NSObject {
    
    fileprivate(set) var fetchedResultsController: NSFetchedResultsController<Value>
    private let observer = CoreDataObserver()
    
    var willChange: (() -> Void)?
    var didChange: (() -> Void)?
    
    var fetchRequest: NSFetchRequest<Value> {
        return fetchedResultsController.fetchRequest
    }
    
    var fetchedObjects: [Value]? {
        return fetchedResultsController.fetchedObjects
    }
    
    var fetchLimit: Int {
        get {
            return fetchRequest.fetchLimit
        }
        set {
            fetchRequest.fetchLimit = newValue
            refetch()
        }
    }
    
    var first: Value? {
        
        guard numberOfSections() > 0 else {
            return nil
        }
        
        guard numberOfObjects(in: 0) > 0 else {
            return nil
        }
        
        return object(at: IndexPath(row: 0, section: 0))
    }
    
    var last: Value? {
        
        guard numberOfSections() > 0 else {
            return nil
        }
        
        let count = numberOfObjects(in: 0)
        
        guard count > 0 else {
            return nil
        }
        
        return object(at: IndexPath(row: count - 1, section: 0))
    }
    
    //MARK: - Lifecicle Object
    
    init(fetchRequest: NSFetchRequest<Value>,
         context: NSManagedObjectContext,
         sectionNameKeyPath: String? = nil,
         cacheName: String? = nil) {
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: context,
                                                              sectionNameKeyPath: sectionNameKeyPath,
                                                                  cacheName: cacheName)
        super.init()
        observer.delegate = self
        fetchedResultsController.delegate = observer
        refetch()
    }
    
    convenience init(fetchRequest: NSFetchRequest<Value>) {
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        self.init(fetchRequest: fetchRequest, context: CoreDataStack.shared.mainContext)
    }
    
    deinit {
        NSFetchedResultsController<Value>.deleteCache(withName: fetchedResultsController.cacheName)
        fetchedResultsController.delegate = nil
    }
    
    //MARK: - Methods
    
    func refetch() {
        NSFetchedResultsController<Value>.deleteCache(withName: fetchedResultsController.cacheName)
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("refetch error \(error)")
        }
    }
    
    func changePredicate(_ predicate: NSPredicate) {
        fetchedResultsController.fetchRequest.predicate = predicate
        refetch()
    }
    
    func changeSortDescriptors(_ sortDescriptors:[NSSortDescriptor]?) {
        fetchedResultsController.fetchRequest.sortDescriptors = sortDescriptors
        refetch()
    }
}


extension CoreDataProvider {
    
    typealias ObjectType = Value
    
    func numberOfSections() -> Int {
        
        if let sections = fetchedResultsController.sections {
            return sections.count
        }
        return 0
    }
    
    func numberOfObjects(in section: Int) -> Int {
        if let section = fetchedResultsController.sections?[section] {
            
            if fetchLimit > 0 {
                return min(fetchLimit, section.numberOfObjects)
            } else {
                return section.numberOfObjects
            }
        }
        return 0
    }
    
    func object(at indexPath: IndexPath) -> Value {
        return fetchedResultsController.object(at: indexPath)
    }
    
    func indexPath(for object: Value) -> IndexPath? {
        return fetchedResultsController.indexPath(forObject: object)
    }
}

extension CoreDataProvider: CoreDataObserverDelegate {
    fileprivate func observerWillChangeContent(_ observer: CoreDataObserver) {
        willChange?()
    }
    
    fileprivate func observerDidChangeContent(_ observer: CoreDataObserver) {
        didChange?()
    }
}

fileprivate protocol CoreDataObserverDelegate: class {
    func observerWillChangeContent(_ observer: CoreDataObserver)
    func observerDidChangeContent(_ observer: CoreDataObserver)
}

fileprivate class CoreDataObserver: NSObject, NSFetchedResultsControllerDelegate {
    weak var delegate: CoreDataObserverDelegate?
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.observerWillChangeContent(self)
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.observerDidChangeContent(self)
    }
}

