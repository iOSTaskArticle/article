//
//  AppEnvironment.swift
//  Advinans
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

enum AppConfiguration : String {
    case Debug = "Debug"
    case Production = "Release"
}

struct AppConfigurationConstants
{
    let configuration: AppConfiguration
    init(configuration: AppConfiguration) {
        self.configuration = configuration
    }
}

struct AppEnvironment {
    static let shared = AppEnvironment()
    let configuration: AppConfiguration
    let constants: AppConfigurationConstants
    let serverUrl: String
    let tokenTimer: Int
    let contentType: String
    private let environment: JSON
    
    private init() {
        let configuration = Bundle.main.infoDictionary?["Configuration"] as? String ?? ""
        self.configuration = AppConfiguration(rawValue: configuration) ?? .Production
        self.constants = AppConfigurationConstants(configuration: self.configuration)
        guard let path = Bundle.main.path(forResource: "AppEnvironment", ofType: "plist") else { fatalError("AppEnviroment.plist was not found") }
        guard let dictionary = NSDictionary(contentsOfFile: path) as? [String:Any] else { fatalError("AppEnviroment.plist must contain a dictionary of configurations") }
        guard let env = dictionary[configuration] as? [String: Any] else { fatalError("AppEnviroment.plist must contain a dictionary for each configuration") }
        environment = JSON.init(env)
        self.serverUrl = environment["serverURL"].stringValue
        self.tokenTimer = environment["tokenTimer"].intValue
        self.contentType = environment["contentType"].stringValue
    }
}
