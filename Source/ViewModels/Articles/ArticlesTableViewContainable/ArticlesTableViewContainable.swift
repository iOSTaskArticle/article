//
//  ArticlesTableViewContainable.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

class ArticlesTableViewContainable: ArticlesTableViewContainableProtocol {
    var articles: [Article]
    var models: [ArticlesCellViewModel] {
        return modelsWith(articles: articles)
    }
    
    required init(articles: [Article]) {
        self.articles = articles
    }
    
    private func modelsWith(articles: [Article]) -> [ArticlesCellViewModel] {
        return articles.map { ArticlesCellViewModel(article: $0) }
    }
}

protocol ArticlesTableViewContainableProtocol {
    var articles: [Article] { get }
    var models: [ArticlesCellViewModel] { get }
    init(articles: [Article])
}
