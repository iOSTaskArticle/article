//
//  ArticlesViewModel.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import CoreData

protocol ArticlesViewModelProtocol: ViewModelProtocol {
    var articles: [Article] { get }
}

class ArticlesViewModel: ArticlesViewModelProtocol, ViewModelContainableProtocol {
    var state: Variable<State> = Variable(.default)
    var successHandler: (() -> Void) = {_ in }
    var errorHandler: ((APIError) -> Void) = {_ in }
    var parameters: [String: Any] { return [:] }
    var articles: [Article] = [Article]()
    var selectedId: Int32? = nil
    var items: Array<ArticlesCellViewModel> {
        let containable = ArticlesTableViewContainable(articles: articles)
        return containable.models
    }
    
    fileprivate let dataProvider: CoreDataProvider<Article> = {
        let fetchRequest: NSFetchRequest<Article> = Article.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        return CoreDataProvider(fetchRequest: fetchRequest, context: CoreDataStack.shared.mainContext)
    }()
    
    //MARK: - Methods
    
    func load() {
        state.value = .loading
        guard ArticleReachability.shared.isReachable.value else {
            if let articles = dataProvider.fetchedObjects {
                state.value = .default
                update(with: articles)
            }
            
            return
        }

        API.getArticles { [weak self] result in
            self?.state.value = .default
            switch result {
                case let .success(articles):
                    self?.update(with: articles)
                case let .failure(error):
                    self?.errorHandler(error)
            }
        }
    }
    
    private func update(with articles: [Article]) {
        self.articles = articles
        successHandler()
    }
}
