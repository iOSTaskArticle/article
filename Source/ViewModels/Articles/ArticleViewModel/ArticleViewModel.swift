//
//  ArticleViewModel.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import CoreData

protocol ArticleViewModelProtocol: ViewModelProtocol {
    var id: Int32 { get }
}

class ArticleViewModel: ArticleViewModelProtocol {
    var state: Variable<State> = Variable(.default)
    var successHandler: ((Article) -> Void) = {_ in }
    var errorHandler: ((APIError) -> Void) = {_ in }
    var parameters: [String: Any] { return [:] }
    var articleSaved: Article? {
        return CoreDataProvider(fetchRequest: Article.fetchRequest(id: id)).fetchedObjects?.first
    }
    var id: Int32 = 0
    var article: Article? = nil
    
    //MARK: - Lifecicle Object
    
    init(id: Int32) {
        self.id = id
    }
    
    //MARK: - Methods
    
    func load() {
        state.value = .loading
        if !ArticleReachability.shared.isReachable.value {
            if let article = articleSaved {
                update(with: article)

            }
            state.value = .default
            return
        }
        
        API.getArticleBody(id: id) { [weak self] result in
            self?.state.value = .default
            switch result {
                case let .success(article):
                    self?.update(with: article)
                case let .failure(error):
                    self?.errorHandler(error)
            }
        }
    }
    
    private func update(with article: Article) {
        self.article = article
        successHandler(article)
    }
}
