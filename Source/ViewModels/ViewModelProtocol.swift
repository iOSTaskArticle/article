//
//  ViewModelProtocol.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

enum State {
    case `default`
    case loading
    case error
}

protocol ViewModelProtocol {
    associatedtype T
    var state: Variable<State> { get set }
    var successHandler: ((T) -> Void) { get set }
    var errorHandler: ((APIError) -> Void) { get set }
    var parameters: [String: Any] { get }
    func load()
}

protocol ViewModelContainableProtocol  {
    associatedtype U
    var items: Array<U> { get }
}
