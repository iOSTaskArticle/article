//
//  NSPersistentStoreCoordinator+Store.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import CoreData

private let maxNumberOfTriesToCreateStore = 3

extension NSPersistentStoreCoordinator {
    
    func addSQLiteStoreWithURL(url: URL, numberOfTries: Int = 0) {
        
        let options = [NSMigratePersistentStoresAutomaticallyOption : true,
                       NSInferMappingModelAutomaticallyOption : true,
                       NSSQLiteManualVacuumOption : true
        ]
        
        do {
            try self.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch let error as NSError {
            
            if error.domain == NSCocoaErrorDomain {
                
                let isMigrationError =
                    error.code == NSPersistentStoreIncompatibleVersionHashError ||
                        error.code == NSMigrationMissingSourceModelError
                
                if isMigrationError {
                    
                    do {
                        try FileManager.default.removeItem(at: url)
                    } catch {
                        
                    }
                    
                    if numberOfTries < maxNumberOfTriesToCreateStore {
                        addSQLiteStoreWithURL(url: url, numberOfTries: numberOfTries + 1)
                    }
                }
            }
        }
    }
}

extension NSPersistentStore {
    
    class func URLForStore(named: String, appGroupIdentifier: String? = nil) -> URL? {
        
        var name = named
        
        if name.contains("sqlite") == false {
            name.append("sqlite")
        }
        
        var URL: URL? = nil
        
        if let appGroupIdentifier = appGroupIdentifier {
            URL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: appGroupIdentifier)
        } else {
            URL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first
        }
        
        URL = URL?.appendingPathComponent(name)
        
        return URL
    }
}
