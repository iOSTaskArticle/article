//
//  NSManagedObjectContext+Save.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectContext {
    
    func saveAsynchronously(asynchronously: Bool = true, completion: (() -> (Void))? = nil) {
        let saveBlock: () -> (Void) = { [weak self] in
            if self?.hasChanges == true {
                do {
                    try self?.save()
                } catch let error as NSError {
                    print("context failed to save with error %@", error)
                }
            }
            
            if let parentContext = self?.parent, parentContext.hasChanges {
                parentContext.saveAsynchronously(asynchronously: asynchronously, completion: completion)
            } else if let completion = completion {
                completion()
            }
        }
        
        if asynchronously {
            perform(saveBlock)
        } else {
            performAndWait(saveBlock)
        }
    }
}


