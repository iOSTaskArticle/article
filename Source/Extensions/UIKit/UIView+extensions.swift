//
//  UIView+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension UIView {
    class var identifier: String {
        let classIdentifier = NSStringFromClass(self)
        guard let indentifier = classIdentifier.lastComponent else { fatalError(Constants.BaseView.errorfoundName + classIdentifier) }
        return indentifier
    }
    var identifier: String {
        let classIdentifier = NSStringFromClass(type(of: self))
        guard let indentifier = classIdentifier.lastComponent else { fatalError(Constants.BaseView.errorfoundName + classIdentifier) }
        return indentifier
    }
    
    //MARK - IBInspectable Methods

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor : UIColor {
        get {
            return UIColor(cgColor: layer.borderColor ?? UIColor.white.cgColor)
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var patternColorImage: String {
        get {
            return ""
        }
        set {
            backgroundColor = UIColor(patternImage: UIImage(named: newValue) ?? UIImage())
        }
    }
}
