//
//  UITextFiled+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get { return self.placeHolderColor }
        set {
            let placeholder = self.placeholder != nil ? self.placeholder! : ""
            self.attributedPlaceholder = NSAttributedString(string:placeholder,
                                                            attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
    @IBInspectable var leftPaddingPoints: CGFloat {
        get { return 0.0 }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
    }
    @IBInspectable var rightPaddingPoints: CGFloat {
        get { return 0.0 }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
    }
}
