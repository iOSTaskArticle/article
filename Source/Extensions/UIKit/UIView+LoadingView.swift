//
//  UIView+LoadingView.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import MBProgressHUD

extension UIView {
    func showProgress() {
        hideProgress()
        MBProgressHUD.showAdded(to: self, animated: true)
    }
    
    func hideProgress() {
        MBProgressHUD.hide(for: self, animated: true)
    }
    
    func progressFor(state: State) {
        if state == .loading { showProgress() }
        else { hideProgress() }
    }
}
