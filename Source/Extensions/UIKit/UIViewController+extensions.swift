//
//  UIViewController+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//


extension UIViewController {

    func present() {
        presenter().present()
    }
    
    func push() {
        presenter().push()
    }
}
