//
//  UILabel+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import SDWebImage

extension UIImageView {

    func image(withURL stringUrl: String?) {
        let placeholderImage = UIImage(named: "icon_no_photo_articles")
        guard let stringUrl = stringUrl, let url = URL(string: stringUrl) else {
            image = placeholderImage
            return
        }
        sd_setImage(with: url, placeholderImage: placeholderImage)
    }
}
