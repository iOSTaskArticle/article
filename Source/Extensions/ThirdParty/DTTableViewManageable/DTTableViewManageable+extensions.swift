//
//  DTTableViewManageable+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension DTTableViewManageable {
    func enableSelfSizing(estimatedHeight: CGFloat = 60) {
        tableView.estimatedRowHeight = estimatedHeight
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}
