//
//  DTModelStorage+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension MemoryStorage {
    open func removeAll() {
        startUpdate()
        removeAllItems()
        sections.removeAll()
        finishUpdate()
    }
}
