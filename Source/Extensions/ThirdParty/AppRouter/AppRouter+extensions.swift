//
//  AppRouter+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension AppRouter {
    
    @discardableResult
    open static func goToRoot<T:UIViewController>(type: T.Type) -> T? {
        guard let viewController = type.instantiate() else { return nil}
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.setNavigationBarHidden(true, animated: true)
        return viewController
    }
    
    @discardableResult
    open static func goToRootWith<T:UIViewController>(viewController: T) -> T? {
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.setNavigationBarHidden(true, animated: true)
        return viewController
    }
    
    @discardableResult
    open static func goTo<T:UIViewController>(type: T.Type) -> T? {
        guard let viewController = type.instantiate() else { return nil }
        return viewController.presenter().push()
    }
    
    open static func popToRoot() {
        AppRouter.topViewController?.navigationController?.popToRootViewController(animated: true)
    }
}
