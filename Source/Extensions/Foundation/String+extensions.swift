//
//  String+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension String {
    var lastComponent: String? {
        return self.components(separatedBy: Constants.BaseView.dot).last
    }
}
