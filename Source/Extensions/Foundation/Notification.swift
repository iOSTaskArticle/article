//
//  Notification.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17..
//  Copyright © 2017 Article. All rights reserved.
//

extension NotificationCenter {
    static func send(name: Notification.Name) {
        NotificationCenter.send(name: name, info: nil)
    }
    
    static func send(name: Notification.Name, info: [AnyHashable : Any]?) {
        NotificationCenter.default.post(name: name, object: nil, userInfo: info)
    }
    
    static func observe(_ observer: Any, selector aSelector: Selector, name aName: NSNotification.Name?) {
        NotificationCenter.default.addObserver(observer, selector: aSelector, name: aName, object: nil)
    }
}

extension Notification.Name {

}
