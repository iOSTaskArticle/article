//
//  CGRect+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension CGRect {
    public init(w: CGFloat, h: CGFloat) {
        self.init(x: 0, y: 0, width: w, height: h)
    }
}
