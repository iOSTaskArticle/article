//
//  String+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 OArticle. All rights reserved.
//

public extension Data {
    
    public func parse(withOptions options: JSONSerialization.ReadingOptions = []) -> JSON? {
        
        guard let json = (try? JSONSerialization.jsonObject(with: self, options: options)) else {
            return nil
        }
        
        return JSON(json)
    }
}
