//
//  NSError+extensions.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension NSError {
    enum Article {
        static func errorWith(description: String) -> NSError {
            let userInfo = [NSLocalizedDescriptionKey: description]
            let error = NSError(domain:"Server", code:0, userInfo:userInfo)
            return error
        }
    }
}
