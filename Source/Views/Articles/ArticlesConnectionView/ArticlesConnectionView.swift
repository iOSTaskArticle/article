//
//  ArticlesConnectionView.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

class ArticlesConnectionView: BaseView {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    var constraintHandler: ((CGFloat) -> Void) = {_ in }

    //MARK: - Methods
    
    override func initializeProperties() {
        closeButton.rx.tap.subscribe(onNext: { [weak self] _ in
            self?.dismiss()
        }).disposed(by: disposeBag)
    }
    
    func present() {
        UIView.animate(withDuration: Constants.Articles.animationDuration, delay: 0.0, options: .curveEaseInOut, animations: {
            self.constraintHandler(CGFloat.greatestFiniteMagnitude)
            self.isHidden = false
            self.layoutIfNeeded()
        })
    }
    
    func dismiss() {
        UIView.animate(withDuration: Constants.Articles.animationDuration, delay: 0.0, options: .curveEaseInOut, animations: {
            self.constraintHandler(-CGFloat.greatestFiniteMagnitude)
            self.isHidden = true
            self.layoutIfNeeded()
        })
    }
}
