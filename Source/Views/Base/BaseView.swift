//
//  BaseView.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

class BaseView: UIView {
    var disposeBag = DisposeBag()
    
    //MARK: Lifecicle Object
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    //MARK: - Method
    
    private func xibSetup() {
        guard let view = viewFromNib() else { return }
        initializeProperties()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    private func viewFromNib() -> UIView? {
        let nib = UINib(nibName: self.identifier, bundle: Bundle(for: type(of: self)))
        let nibs = nib.instantiate(withOwner: self, options: nil)
        return nibs.first as? UIView
    }
    
    func initializeProperties() { }
}
