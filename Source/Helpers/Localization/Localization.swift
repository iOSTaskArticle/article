//
//  Localization.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

enum Localization {
    enum Common {
        static let cancel = NSLocalizedString("Cancel", comment: "Common: Cancel")
        static let error = NSLocalizedString("Error", comment: "Common: Error")
        static let ok = NSLocalizedString("OK", comment: "Common: OK")
        static let dismiss = NSLocalizedString("Dismiss", comment: "Common: Dismiss")
        static let yes = NSLocalizedString("Yes", comment: "Common: Yes")
        static let no = NSLocalizedString("No", comment: "Common: No")
        static let other = NSLocalizedString("Other", comment: "Common: Other")
        static let article = NSLocalizedString("Article", comment: "Common: Name of app for alert")
    }
}
