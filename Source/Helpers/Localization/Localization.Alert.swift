//
//  Localization.Alert.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

extension Localization {
    enum Alert {
        static let errorTitle = Localization.Common.error
        static let ok = Localization.Common.ok
        static let dismiss = Localization.Common.dismiss
        static let yes = Localization.Common.yes
        static let no = Localization.Common.no
    }
}
