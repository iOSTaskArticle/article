//
//  Reachability.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import Alamofire

class ArticleReachability {
    static let shared = ArticleReachability()
    var isReachable: Variable<Bool> = Variable(false)
    private let reachability = NetworkReachabilityManager()
    
    func startListening() {
        reachability?.startListening()
        reachability?.listener = { [unowned self] status in
            self.isReachable.value = self.reachability?.isReachable ?? false
            print("<< isReachable = ", self.isReachable.value)
        }
    }
}
