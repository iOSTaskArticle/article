//
//  Alert.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

class Alerts {
    
    class func defaultAlert(message: String) {
        if message.characters.count == 0 { return }
        showAlert(Localization.Common.article, message: message)
    }
    
    class func showAlert(_ title: String?, message: String) {
        if message.characters.count == 0 { return }
        
        let defaultAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        defaultAlert.addAction(UIAlertAction(title: Localization.Alert.ok, style: .cancel, handler: nil ))
        defaultAlert.present()
    }
    
    class func showAlertWithDismiss(_ title: String?, message: String) {
        let defaultAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        defaultAlert.addAction(UIAlertAction(title: Localization.Alert.dismiss, style: .cancel, handler: nil ))
        defaultAlert.present()
    }
}
