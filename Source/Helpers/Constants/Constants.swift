//
//  Constants.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

enum Constants {
    enum API {
        enum Header {
            static let accept = "Accept"
            static let contentType = "Content-Type"
            static let authorization = "Authorization"
        }
    }
    
    enum BaseView {
        static let errorfoundName = "Class don`t found with name"
        static let dot = "."
    }
    
    enum Articles {
        static let animationDuration = 0.3
    }
}
