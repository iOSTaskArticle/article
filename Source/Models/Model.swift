//
//  Model.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

protocol Model {
    init(json: JSON)
}
