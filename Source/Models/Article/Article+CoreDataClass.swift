//
//  Article+CoreDataClass.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import CoreData

@objc(Article)
public class Article: NSManagedObject {
    
    static func fetchRequest(id: Int32) -> NSFetchRequest<Article> {
        let fetchRequest = NSFetchRequest<Article>(entityName: "Article")
        fetchRequest.fetchLimit = 1
        fetchRequest.includesSubentities = false
        fetchRequest.predicate = NSPredicate(format: "id == \(id)")
        return fetchRequest
    }
    
    func export() -> String {
        return [title, contentUrl].reduce("") { (result, string) -> String in
            guard let string = string else { return result }
            return string.isEmpty ? result : String(format: "%@ %@", result, string)
        }
    }
}

extension Article: ManagedObjectJSONParsable {
    
    static func parse(json: JSON, context: NSManagedObjectContext) -> Article? {

        guard let id = json["id"].int32 else { return nil }
        let fetchRequest = Article.fetchRequest(id: id)
        var entity: Article? = nil

        context.performAndWait {
            do {
                entity = try context.fetch(fetchRequest).first
            } catch {}
        }

        if entity == nil {
            entity = NSEntityDescription.insertNewObject(forEntityName: "Article", into: context) as? Article
        }

        guard let article = entity else {
            return nil
        }

        article.id = id
        article.title = json["title"].string
        article.imageMedium = json["image_medium"].string
        article.imageThumb = json["image_thumb"].string
        article.contentUrl = json["content_url"].string

        return article
    }
}

extension Article: ManagedObjectStringParsable {

    static func parse(id: Int32, string: String, context: NSManagedObjectContext) -> Article? {
        
        var entity: Article? = nil
        let fetchRequest = Article.fetchRequest(id: id)
        
        context.performAndWait {
            do {
                entity = try context.fetch(fetchRequest).first
            } catch {}
        }
        
        guard let article = entity else {
            return nil
        }
        
        article.html = string
        
        return article
    }
}



