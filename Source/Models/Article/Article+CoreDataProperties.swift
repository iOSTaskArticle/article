//
//  Article+CoreDataProperties.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import CoreData

extension Article {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        return NSFetchRequest<Article>(entityName: "Article")
    }

    @NSManaged public var id: Int32
    @NSManaged public var title: String?
    @NSManaged public var imageMedium: String?
    @NSManaged public var imageThumb: String?
    @NSManaged public var contentUrl: String?
    @NSManaged public var html: String?

}

