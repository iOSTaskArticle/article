//
//  API.Articles.swift
//  Article
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import Alamofire

extension API {
    static func getArticles(closure: @escaping (Result<[Article], APIError>) -> Void) {
        let request = API.getRequest(with: "application/ios_test_task/articles")
        request.responseJSON { (response) in
            let parseObject = ManagedObjectsResponseParser<Article>()
            let result = parseObject.parse(request: request.request,
                                     response: response.response,
                                     data: response.data,
                                     error: response.error)
            closure(result)
        }
    }
    
    static func getArticleBody(id: Int32, closure: @escaping (Result<Article, APIError>) -> Void) {
        let request = API.getRequest(with: "articles/\(id)", encoding: URLEncoding.default)
        
        request.responseString { (response) in
            let parseObject = ManagedObjectResponseParser<Article>(id: id)
            let result = parseObject.parse(request: request.request,
                                           response: response.response,
                                           data: response.data,
                                           error: response.error)
            closure(result)
        }
    }
}

