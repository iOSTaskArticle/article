//
//  API.swift
//  Article
//
//  Created by Oleksandr Nesynov on 14.12.17.
//  Copyright © 2017 Article. All rights reserved.
//

import Alamofire

enum API {
    typealias Parameters = [String : Any]
    private static let url = AppEnvironment.shared.serverUrl
    static func headers() -> HTTPHeaders {
        return [Constants.API.Header.contentType : AppEnvironment.shared.contentType]
    }
    
    static func getRequest(with path: String, parameters: [String : Any]? = nil, encoding: ParameterEncoding = JSONEncoding.default) -> DataRequest {
        return API.request(with: path, method: .get, parameters: parameters, encoding: encoding)
    }
    
    private static func request(with path: String, method: HTTPMethod, parameters: [String : Any]? = nil, encoding: ParameterEncoding) -> DataRequest {
        return Alamofire.request(API.url + path,
                                        method: method,
                                        parameters: parameters,
                                        encoding: encoding,
                                        headers: API.headers())
    }
}
